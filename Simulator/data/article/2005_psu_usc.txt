I had originally planned to write about 2005 Texas vs 2005 Penn State, but the more I looked the numbers, the less and less interested I was at writing about that hypothetical match.  2005 Texas was too good.  They were way, way too good compared to other teams that year.  Texas' Modified PageRank score in 2005 was 602.47, Penn State came in #2 from this model with a much lower 345.46.  This is much larger than the gap between Ohio State and #2 this year.  One would expect this years gap to be much larger because of the playoff massively boosting the winners strength of schedule.   Additionally the MLE gap between Texas and Penn State was 166 points in Texas favor.  When I ran the numbers through the points only model, Texas won 78% of the time with an average score of 33-20.  When using the play by play model, Texas won 77% of the time by a margin of 34-19.   Because of this huge disparity I decided to look at '05 USC vs '05 Penn State. 
***
 Now this write up will be a lot less technical and in depth than the articles for current teams.  This is because of missing data and lack of complete data for the FCS and lower.  If anyone knows where I can find at least scores for every game please let me know and I will try to get back to the deeper details. But enough introduction, lets look at our competitors.
***
2005 USC was one of the most explosive, high power offenses of all time.  The offense was lead by Heisman trophy winners Matt Leinhart and Reggie Bush and run by the brilliant minds of Lane Kiffin and Pete Carroll.  Coming off a National championship in 2004, USC came into the season ranked #1 and a a considerable favorite to win the national championship.  Throughout the season they showed they truly deserved that consideration too.  The entire season the lowest score USC put up was 34 points at South Bend against #9(then and end of season) Notre Dame.  Over the course 
the 13 game season, USC averaged 49.1 points per game.  They didn't put up big numbers by playing slouches either.  They put up 66 points against #13 UCLA and 45 points against #10 Oregon(end of season rankings). On top of that, USC had a strong defense.  They only allowed over 30 points in 3 games.  They only allowed on average a very solid 22.8 points per game.  This may have been one of the best teams of all time and they still weren't the best team in 2005.
***
In 2005, Penn State was much the opposite of USC. Coming in off a 4-7 2004 campaign, PSU came into the season unranked and with low expectations.  Their defense stepped up and blew those expectations away.  Even when the offense was stagnant Penn State's stalwart defense propelled them to an 11-1 season.  Their only loss which kept them out of the national championship game came to a mediocre Michigan who went a mere 7-5.  Penn State's offense in 2005 wasn't explosive or high powered by any stretch of the imaginiation, but it was highly consistent.  They scored under 30 points in only 4 games all year long, however they only hit or broke 40 points in 4 games as well. All said and done on the season PSU allowed a mere 17 points per game and had an average margin of victory of 17.4 points per game. This means on average PSU doubled their opponents score. The Nittany Lions came just 2 points shy of potentially playing USC in Texas place. 
***
Let's look a bit at the numbers for each team on the season.

Team | PTS For/G | Pass For/G | Rush For/G | PTS Against/G | Pass Against/G | Rush Against/G
----|---------|----------|----------|-------------|--------------|--------------
[](#f/usc) USC | 49.1 | 319.8 | 260 | 22.8 | 230.4 | 130.5
 [](#f/pennstate) Penn State  | 34.4 | 208.8 | 212.8 | 17.0 | 211.7 | 93.0
Advantage |  [](#f/usc) USC |[](#f/usc) USC  |[](#f/usc) USC  | [](#f/pennstate) Penn State  | [](#f/pennstate) Penn State  | [](#f/pennstate) Penn State 
Adv %| 42.6% | 53.2% | 22.2% | 25.6% | 8.1% | 28.8%


As expected, we see that Penn State had a dominate defense in every fashion, but USC had an unparallelled offense.  Obviously the raw numbers aren't the be all end all so let's look at the comparison to the opponent's averages.

Forgive for these being in a different order than the above. It is a major hassle to correct.

Team|% Yards Above Opponents Average Allowed|% Rushing Yards Above Opponents Average Allowed|% Points Above Opponents Average Allowed|% Passing Yards Allowed Above Opponents Average Gained|% Rushing Yards Allowed Above Opponents Average Gained|% Points Allowed Opponents Average Scored
-|-|-|-|-|-|-
[](#f/usc) USC |125.5%|159.4%|176.2%|82.0%|88.6%|71.3%
 [](#f/pennstate) Penn State |107.2%|136.6%|138.8%|77.4%|54.0%|60.7%
Advantage |  [](#f/usc) USC |[](#f/usc) USC  |[](#f/usc) USC  | [](#f/pennstate) Penn State  | [](#f/pennstate) Penn State  | [](#f/pennstate) Penn State 
Adv %|17.1%|16.7%|26.9%|5.6%|39.1%|14.9%

These numbers reinforce what we knew but makes it makes the gap on both sides a little smaller. It also illustrates just how hopeless running the ball against what may have been the best run defense of all time truly was.  USC still holds the over all edge in the points, but it is much smaller than it appeared from the raw numbers.
***
There are more numbers to look at as well though.  Let's look at a few of those x-factor style stats for each team.


Team | Penalty/G | Turnovers For/G | Turnovers Against/G | Margin/G
----|---------|---------------|-------------------|--------
[](#f/usc) USC | 63.6 | 2.92 | 1.31 | +1.62
[](#f/pennstate) Penn State | 35.2 | 2.08 | 1.83 | +0.25
Advantage | [](#f/pennstate) Penn State | [](#f/usc) USC | [](#f/usc) USC | [](#f/usc) USC
Adv % | 44.6% | 40.3% | 28.7% | N/A

Here we see USC has a huge advantage in the turnover margin in exchange for a small hit in the penalty margin.  This is a huge advantage for USC and is the saving grace of their defense.  Even against the best defenses, turnovers result in points.  Keeping the opponent's defense on the field is the fastest way to win a game.

Let's look at strength of schedule as another x-factor.

Team | SoS
----|---
[](#f/usc) USC | 0.5564
[](#f/pennstate) Penn State | 0.5481
Advantage | [](#f/usc) USC
Adv % | 1.5%

This is another small advantage for USC.  They played a slightly strong schedule without taking into account any conference adjustments which would likely favor USC farther.  This advantage is almost negligible though as it results in about a 0.76% adjustment modifier to the overall stats in favor of USC and against Penn State.

Finally we can look at two of my historical ranking systems MLE(most likely elo) and MPR(modified PageRank).  This two models together give a good idea of how good various teams are.  Without lower division data the MPR model is only about 79.7% accurate for predicting the winner where as the retrograde analysis showed the final MLE rankings selected the correct winner 81.4% of the time.

Team | MLE Score | MLE Rank | MPR Score | MPR Rank
----|---------|--------|---------|--------
[](#f/usc) USC | 2208.2 | #2 | 174.8 | #10
[](#f/pennstate) Penn State | 2105.2 | #4 | 345.5 | #2
Advantage | [](#f/usc) USC | [](#f/usc) USC | [](#f/pennstate) Penn State | [](#f/pennstate) Penn State | [](#f/pennstate) Penn State | [](#f/pennstate) Penn State

Since MPR is primarily a measure of strength of wins it means that Penn State had better wins, but the much worse loss as reflected in the the MLE model.  Bare in mind that MPR is not a linear ranking system.  It decays nearly exponentially and the gap between #3 and #10 is almost nonexistent and losses don't matter nearly as much as big wins.  These numbers slightly favor Penn State but not by much.

***
So let's jump into the simulations.

The first simulation is points only model.

Team|Passing Yards|Rushing Yards|Penalty Yards|Turnovers|Points|Wins
-|-|-|-|-|-|-
[](#f/usc) USC |264.3|148.86|52.25|1.59|31.07|5693
[](#f/pennstate) Penn State |178.93|202.28|29.82|2.02|27.77|4307

We see that the model projects Penn State to be the team to finally slow down USC, but their offense just can't get the numbers they need to take the win.  This would have been an incredibly exciting game to watch.  

Next we run the full play by play model.

Team|Passing Yards|Rushing Yards|Penalty Yards|Turnovers|Points|Wins
-|-|-|-|-|-|-
[](#f/usc) USC |265.16|151.83|52.35|1.62|31.52|5712
[](#f/pennstate) Penn State |178.06|203.82|29.87|2.02|27.80|4288

The full play by play model always produces very similar results but usually shift about 3-5% one way or the other because a one team has a huge advantage somewhere the other side can't match.  That didn't happen this time, this is indicative of two teams that match up very well against one another.  Each teams strength is almost completely countered by the others advantage somewhere else.  Here USC's unstoppable offense is slowed down by taking away the run game and Penn State can move the ball against USC.  USC pulls slightly farther ahead, but the game is still about as close as one could ask for.

***
Overall, I think people were right to tell me to forget about Texas Penn State, I don't think that game would have been as bad in reality as people seem to think, but it certainly wouldn't be falling the Nittany Lions way most days.  USC versus Penn State is a much better match in almost every fashion.  These teams are built to counteract one another it seems and if Penn State had beaten Michigan likely would have resulted in one of the most exciting BCS championships of all time.  Ultimately, it may have been for the best though as the 2006 Rose Bowl is one of the most exciting games of all time in its own right and gave Vince Young his second Rose Bowl MVP in a game featuring 4(!) top 10 picks in the 2006 NFL Draft.  Fate left Penn State out and it may have been for the best as Texas USC is one of the greatest BCS championships.  Ultimately I would take '05 USC over '05 Penn State with a predicted final score of 31-28.

***
Sorry this write-up is much abridged compared to the 2014 write-ups but finding enough film and data was very difficult and this was before my days of watching every game so I only had about 6 games I watched live to go on between both schools.

